# [*] Danitsu

import subprocess
import csv
import sys
import os
from datetime import date

def lecturaInput():
	f = open("inputSMB.txt", "r")
	val = 0
	for x in f:
		val += 1
		line = x.rstrip()
		if val == 1:
			user = line
		elif val == 2:
			passw = line
		elif val == 3:
			case = line
		elif val == 4:
			archivo = line
		elif val == 5:
			domain = line
		else:
			print('')
			print('[*][-] Error - Revise nuevamente el archivo inputSMB.txt " ')
			print('')
			sys.exit(1)


	smb(user, passw, case, archivo, domain)

def smb(user, passw, case, archivo, domain):
	datasmb = {}
	try: 
		print ('[*] Working ')
		f = open(archivo, "r")
		for x in f:
			line = x.rstrip()
			## Caso 1 - indicando la mascara
			if(case == '1'):
				line = line.split('/')
				splitIP = line[0].split('.')
				mask = str(line[1])
				## Mascara /16
				if(mask == '16'):
					print ('[*] Mascara /16 ')
					for y in range(0,256):
						for x in range(0,256):
							IP = str(splitIP[0]) + '.' + str(splitIP[1]) + '.' + str(x) + '.' + str(y)
							smbmap = "smbmap -H "+ IP + " -d "+ domain + " -u '"+ user +"' -p '"+ passw +"'"
							p1 = subprocess.Popen(smbmap, shell=True,stdout=subprocess.PIPE)
							stdoutsmb = p1.communicate()[0]
							key = IP
							value = stdoutsmb
							datasmb[key] = value
							arr = value
				## Mascara /21
				elif(mask == '21'):
					print ('[*] Mascara /21 ')
					for y in range(0,256):
						for x in range(248,256):
							IP = str(splitIP[0]) + '.' + str(splitIP[1]) + '.' + str(x) + '.' + str(y)
							smbmap = "smbmap -H "+ IP + " -d "+ domain + " -u '"+ user +"' -p '"+ passw +"'"
							p1 = subprocess.Popen(smbmap, shell=True,stdout=subprocess.PIPE)
							stdoutsmb = p1.communicate()[0]
							key = IP
							value = stdoutsmb
							datasmb[key] = value
							arr = value
				## Mascara /23
				elif(mask == '23'):
					print ('[*] Mascara /23 ')
					for y in range(0,256):
						for x in range(254,256):
							IP = str(splitIP[0]) + '.' + str(splitIP[1]) + '.' + str(x) + '.' + str(y)
							smbmap = "smbmap -H "+ IP + " -d "+ domain + " -u '"+ user +"' -p '"+ passw +"'"
							p1 = subprocess.Popen(smbmap, shell=True,stdout=subprocess.PIPE)
							stdoutsmb = p1.communicate()[0]
							key = IP
							value = stdoutsmb
							datasmb[key] = value
							arr = value
				## Mascara /24
				elif(mask == '24'):
					print ('[*] Mascara /24 ')
					for x in range(0,256):
						IP = str(splitIP[0]) + '.' + str(splitIP[1]) + '.' + str(splitIP[2]) + '.' + str(x)
						smbmap = "smbmap -H "+ IP + " -d "+ domain + " -u '"+ user +"' -p '"+ passw +"'"
						p1 = subprocess.Popen(smbmap, shell=True,stdout=subprocess.PIPE)
						stdoutsmb = p1.communicate()[0]
						key = IP
						value = stdoutsmb
						datasmb[key] = value
						arr = value
				## Mascara /30
				elif(mask == '30'):
					print ('[*] Mascara /30 ')
					for x in range(252,256):
						IP = str(splitIP[0]) + '.' + str(splitIP[1]) + '.' + str(splitIP[2]) + '.' + str(x)
						smbmap = "smbmap -H "+ IP + " -d "+ domain + " -u '"+ user +"' -p '"+ passw +"'"
						p1 = subprocess.Popen(smbmap, shell=True,stdout=subprocess.PIPE)
						stdoutsmb = p1.communicate()[0]
						key = IP
						value = stdoutsmb
						datasmb[key] = value
						arr = value
				## Mascara /31
				elif(mask == '31'):
					print ('[*] Mascara /31 ')
					for x in range(254,256):
						IP = str(splitIP[0]) + '.' + str(splitIP[1]) + '.' + str(splitIP[2]) + '.' + str(x)
						smbmap = "smbmap -H "+ IP + " -d "+ domain + " -u '"+ user +"' -p '"+ passw +"'"
						p1 = subprocess.Popen(smbmap, shell=True,stdout=subprocess.PIPE)
						stdoutsmb = p1.communicate()[0]
						key = IP
						value = stdoutsmb
						datasmb[key] = value
						arr = value
				else: 
					print('')
					print('[*][-] Mascara incorrecta')
					print('')
					sys.exit(1)

			## Caso 2 - indicando las IPs
			elif(case == '2'):
				IP = str(line)
				smbmap = "smbmap -H "+ IP + " -d "+ domain + " -u '"+ user +"' -p '"+ passw +"'"
				p1 = subprocess.Popen(smbmap, shell=True,stdout=subprocess.PIPE)
				stdoutsmb = p1.communicate()[0]
				key = IP
				value = stdoutsmb
				datasmb[key] = value
				arr = value
				#print(arr)
			else:
				print('')
				print('[*][-] Opcion incorrecta - Favor modificar linea 3 de "inputSMB.txt" ')
				print('')
				sys.exit(1)

		## Guardando respuesta en Temporal 
		out = 'outputsmb_'
		today = date.today()
		name = out + str(today)
		namefile = name 
		print('fecha')
		print(namefile)

		with open(name, 'w') as csv_file:  
			writer = csv.writer(csv_file)
			for key, value in datasmb.items():
				writer.writerow([value])
		
		print('')
		print('[*][+] Archivo guardado en ' + namefile)
		print('') 
	except OSError as e:
		print('')
		print("[*][-] Error")
		print('')
		sys.exit(1)


if __name__ == '__main__':
	print('')
	print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
	print('')
	print '         [*]  Recuerde que antes de comenzar debe leer el README  ' 
	print '         [*]  Favor luego de leer el README actualizar el archivo inputSMB.txt ' 
	print('')
	print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
	print('')

	lecturaInput()

	print('')
	print('[*][+] Archivo guardado con exito ')
	print('')       
